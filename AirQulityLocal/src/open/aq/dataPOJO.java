package open.aq;

import java.io.Serializable;

public class dataPOJO implements Serializable {
    private String _id, location, value, unit, parameter, country, city;

    public dataPOJO(String _id, String location, String value, String unit, String parameter, String country, String city){
        this._id = _id;
        this.location = location;
        this.value = value;
        this.unit = unit;
        this.parameter = parameter;
        this.country = country;
        this.city = city;
    }


    public String get_id() {
        return _id;
    }

    public String getLocation() {
        return location;
    }

    public String getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }

    public String getParameter() {
        return parameter;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }
}
