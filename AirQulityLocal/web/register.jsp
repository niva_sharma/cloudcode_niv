<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
  <link rel="stylesheet" href="style.css">
  <script defer src="https://use.fontawesome.com/releases/v5.0.7/js/all.js"></script>
  <title>Open AQ Registeration</title>
</head>

<body>
  <!-- Navbar used from bootstrap-->
  <nav class="navbar navbar-dark bg-dark">
    <!-- Navbar content -->
    <div class="container">
        <a class="navbar-brand" href="index.jsp">Air Quality Bot</a>
        
  
        
        </div>
      </div>
  </nav>

  <!--mid section of the page-->
  
 
  <div class="register">
    <div class="container">
      <div class="row">
        <div class="col-md-8 m-auto">
          <h1 class="display-4 text-center">Sign Up</h1>
          <p class="lead text-center">Join and get latest data</p>

          <form action="retrieve" method="post">
            <div class="form-group">
              <input type="text" class="form-control form-control-lg" placeholder="First Name" name="fname" required />
            </div>
            <div class="form-group">
              <input type="text" class="form-control form-control-lg" placeholder="Last Name" name="lname" required />
            </div>
            <div class="form-group">
              <input type="email" class="form-control form-control-lg" placeholder="Email Address" name="email" />
            </div>
            <div class="form-group">
              <input type="text" class="form-control form-control-lg" placeholder="Country" name="country" required />
            </div>
            <div class="form-group">
              <input type="text" class="form-control form-control-lg" placeholder="Age" name="age" required />
            </div>
            <input type="submit" class="btn btn-info btn-block mt-4" />
          </form>
        </div>
      </div>
    </div>
  </div>
  <br/>
  


  
      <script src="http://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
      <!-- stats.js lib -->
      <script src="http://threejs.org/examples/js/libs/stats.min.js"></script>
      <script src="index.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>

</body>

</html>