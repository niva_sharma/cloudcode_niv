let draw = false;

init();

function init() {
    const table = $("#dt-table").DataTable();
    const tableData = getTableData(table);
    // createPie();
   createHighcharts(tableData);
   setTableEvents(table);
}

function getTableData(table) {
    const dataArray = [],
        countryArray = [],
        valueArray = [],
        paraArray = [],
        unitArray = [];

    //loops withing rows
    table.rows({search: "applied"}).every(function() {
        const data = this.data();
        countryArray.push(data[6]);
        valueArray.push(parseInt(data[2].replace(/\,/g, "")));
        unitArray.push(parseInt(data[3].replace(/\,/g, "")));
        paraArray.push(data[4]);
    });

    //store data into this array
    dataArray.push(countryArray, valueArray, unitArray, paraArray);
    return dataArray;
}

function createHighcharts(data) {
    Highcharts.setOptions({
        lang: {
            thousandsSep: ","
        }
    });

    Highcharts.chart("chart", {
        title: {
            text: "Air Quality Pollution"
        },
        subtitle: {
            text: "Air Quality Data"
        },
        xAxis: [
            {
                categories: data[0],
                labels: {
                    rotation: -45
                }
            }
        ],
        yAxis: [
            {
                // first yaxis
                title: {
                    text: "Value (2018)"
                }
            },
            {
                // secondary yaxis
                title: {
                    text: "Unit (P/Km²)"
                },
                min: 0,
                opposite: true
            }
        ],
        series: [
            {
                text: data[3],
                name: "Value",
                color: "#0071A7",
                type: "column",
                data: data[1]
/*
                tooltip: {
                    valueSuffix: " M"
                }
*/
            },
            {
                name: "Unit",
                color: "#FF404E",
                type: "spline",
                data: data[2],
                yAxis: 1
            }
        ],
        tooltip: {
            shared: true
        },
        legend: {
            backgroundColor: "#ececec",
            shadow: true
        },
        credits: {
            enabled: false
        },
        noData: {
            style: {
                fontSize: "16px"
            }
        }
    });
}

function createPie() {
    Highcharts.chart('container', {
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Browser market shares in January, 2018'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: [{
                name: 'Chrome',
                y: 61.41,
                sliced: true,
                selected: true
            }, {
                name: 'Internet Explorer',
                y: 11.84
            }, {
                name: 'Firefox',
                y: 10.85
            }, {
                name: 'Edge',
                y: 4.67
            }, {
                name: 'Safari',
                y: 4.18
            }, {
                name: 'Sogou Explorer',
                y: 1.64
            }, {
                name: 'Opera',
                y: 1.6
            }, {
                name: 'QQ',
                y: 1.2
            }, {
                name: 'Other',
                y: 2.61
            }]
        }]
    });
}


function setTableEvents(table) {
    // listen for page clicks
    table.on("page", () => {
        draw = true;
});

    // listen for updates and adjust the chart accordingly
    table.on("draw", () => {
        if (draw) {
            draw = false;
        } else {
            const tableData = getTableData(table);
    createHighcharts(tableData);
        }
    });
}
